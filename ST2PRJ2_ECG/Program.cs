﻿using System;
using System.IO;
using RaspberryPiCore.LCD;

namespace ST2PRJ2_ECG
{
    class Program
    {
        private static SerLCD _serLcd;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            _serLcd = new SerLCD();

            _serLcd.lcdPrint("Hello World!");

            var sampler = new Sampler(500);

            //Console.WriteLine("Start sample: " +DateTime.Now.TimeOfDay);
            //var samples = sampler.Sample(10);
            //Console.WriteLine("Stop sample: " + DateTime.Now.TimeOfDay);

            //using (var streamWriter = new StreamWriter("samples.csv"))
            //{
            //    foreach (var sample in samples)
            //    {
            //        streamWriter.Write(sample + ",");
            //    }
            //}

            //Console.WriteLine(long.MaxValue);

            Console.Write("Enter data source: ");
            var dataSource = Console.ReadLine();
            Console.WriteLine();

            Console.Write("Enter user id: ");
            var userId = Console.ReadLine();
            Console.WriteLine();

            Console.Write("Enter password");
            var pwd = Console.ReadLine();
            Console.WriteLine();

            var dbWriter = new DbWriter(pwd, userId, "TestBLG", dataSource);

            var id = dbWriter.Write("Bjarke");

            Console.WriteLine("Entry was written to ID: " + id);

            Console.WriteLine("Done!");
        }
    }
}
