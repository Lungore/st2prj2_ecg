﻿using System;
using RaspberryPiCore.ADC;

namespace ST2PRJ2_ECG
{
    class Sampler
    {
        private readonly ADC1015 _adc;
        private readonly double _sampleFrequency;
        private readonly double _deltaTime;

        public Sampler(int sampleFrequency)
        {
            _sampleFrequency = (double)sampleFrequency;
            _adc = new ADC1015();

            _deltaTime = 1 / (double)_sampleFrequency;
        }

        public ushort[] Sample(double duration)
        {
            var numberOfSamples = (int)Math.Round((double)(duration * _sampleFrequency), 0);
            var samples = new ushort[numberOfSamples];
            long lastSampleTime = 0;
            var sampleCount = 0;
            double deltaTime = _deltaTime * 1000000000; 

            //Console.WriteLine("Started sampling at: " + DateTime.Now.TimeOfDay);
            while (sampleCount < numberOfSamples)
            {
                //Console.WriteLine(DateTime.Now.Ticks * 100);
                //Console.WriteLine(DateTime.Now.Ticks * 100 - lastSampleTime);
                //Console.WriteLine(DateTime.Now.TimeOfDay);
                if (DateTime.Now.Ticks * 100 - lastSampleTime >= (long)(deltaTime))
                {
                    lastSampleTime = DateTime.Now.Ticks * 100;
                    //Console.WriteLine("Last sample time: " + lastSampleTime);
                    //Console.WriteLine("Sample at: " + lastSampleTime);

                    samples[sampleCount++] = _adc.readADC_SingleEnded(0);
                }
            }
            //Console.WriteLine("Stopped sampling at: " + DateTime.Now.TimeOfDay);

            return samples;
        }
    }
}
