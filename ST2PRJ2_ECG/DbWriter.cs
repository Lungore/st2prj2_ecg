﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;

namespace ST2PRJ2_ECG
{
    class DbWriter
    {

        private SqlConnection _conn; 

        public DbWriter(string userPwd, string userId, string catalog, string dataSource)
        {

            _conn = new SqlConnection(
                @"Data Source=" + dataSource + 
                ";Initial Catalog=" + catalog +
                ";Integrated Security=False" +
                ";User ID=" + userId +
                ";Password=" + userPwd +
                ";Connect Timeout = 15"+
                ";Encrypt = False"+
                ";TrustServerCertificate = False");
        }

        public long Write(string param)
        {
            long id = -1;
            var insertParams = @"INSERT INTO TestBLG(name) "
                               + "OUTPUT INSERTED.Id" 
                               + "VALUES(@name)";
            using(SqlCommand cmd = new SqlCommand(insertParams, _conn))
            {
                cmd.Parameters.AddWithValue("@name", param);

                _conn.Open();
                id = (long) cmd.ExecuteScalar();
                _conn.Close();
            }

            return id;
        }
    }
}
